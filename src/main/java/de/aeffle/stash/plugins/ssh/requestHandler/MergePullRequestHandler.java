package de.aeffle.stash.plugins.ssh.requestHandler;

import java.io.IOException;

import com.atlassian.bitbucket.pull.PullRequestMergeRequest;
import com.atlassian.bitbucket.pull.PullRequestService;

import com.atlassian.bitbucket.repository.Repository;
import de.aeffle.stash.plugins.ssh.helper.MergeCommand;
import de.aeffle.stash.plugins.ssh.helper.SshShellHandle;
import de.aeffle.stash.plugins.ssh.helper.BitbucketServiceHandler;

import javax.annotation.Nonnull;


public class MergePullRequestHandler extends AbstractRequestHandler {
    private int repositoryId;
    private long pullRequestId;
    private int version;

    public MergePullRequestHandler(String remoteCommand,
                                   SshShellHandle sshShellHandle,
                                   BitbucketServiceHandler bitbucketServiceHandler) {

        super(remoteCommand, sshShellHandle, bitbucketServiceHandler);

        try {
            MergeCommand mergeCommand = new MergeCommand(bitbucketServiceHandler, remoteCommand);

            repositoryId = mergeCommand.getRepositoryId();
            pullRequestId = mergeCommand.getPullRequestId();
            version = mergeCommand.getPullRequestVersion();
        }
        catch (Exception e) {
            handleError(e);
        }
    }


    @Override
    public void handleRequest() throws IOException {
        try {
            mergePullRequest(repositoryId, pullRequestId, version);
            sshShellHandle.writeOutput("Successfully merged the pull-request.");
        } 
        catch (Exception e) {
            handleError(e);
        }
        
    }


    private void mergePullRequest(int repositoryId, long pullRequestId, int version) {
        PullRequestService pullRequestService = bitbucketServiceHandler.getPullRequestService();
        PullRequestMergeRequest pullRequestMergeRequest = new PullRequestMergeRequest.Builder(repositoryId, pullRequestId, version).build();
        pullRequestService.merge(pullRequestMergeRequest);
    }


    @Nonnull
    @Override
    public Repository getRepository() {
        return repositoryService.getById(repositoryId);
    }
}
