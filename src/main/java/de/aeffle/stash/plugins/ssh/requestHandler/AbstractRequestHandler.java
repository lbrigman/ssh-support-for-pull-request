package de.aeffle.stash.plugins.ssh.requestHandler;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Map;

import com.atlassian.bitbucket.repository.RepositoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.bitbucket.i18n.KeyedMessage;
import com.atlassian.bitbucket.pull.InvalidPullRequestReviewersException;
import com.atlassian.bitbucket.scm.ssh.SshScmRequest;

import de.aeffle.stash.plugins.ssh.helper.SshShellHandle;
import de.aeffle.stash.plugins.ssh.helper.BitbucketServiceHandler;

import javax.annotation.Nonnull;


public abstract class AbstractRequestHandler implements SshScmRequest {
    protected static final Logger log = LoggerFactory
			.getLogger(AbstractRequestHandler.class);
	
    protected final BitbucketServiceHandler bitbucketServiceHandler;
    protected final SshShellHandle sshShellHandle;
    protected final String remoteCommand;
    protected final RepositoryService repositoryService;
	

    public AbstractRequestHandler(String remoteCommand,
                                  SshShellHandle sshShellHandle,
                                  BitbucketServiceHandler bitbucketServiceHandler) {

        this.remoteCommand = remoteCommand;
        this.sshShellHandle = sshShellHandle;
        this.bitbucketServiceHandler = bitbucketServiceHandler;
        this.repositoryService = bitbucketServiceHandler.getRepositoryService();
    }

    protected void handleError(Exception e) {
        try {
            String errorKey = e.getClass().getSimpleName();
            errorKey = errorKey.replaceAll("Exception", "");
            errorKey = errorKey.replaceAll("(?!^)([A-Z])", " $1");

            String errorMessage = "\n";
            errorMessage += "ERROR - " + errorKey + ":\n";

            if (e.getClass() == InvalidPullRequestReviewersException.class) {
                InvalidPullRequestReviewersException ipe = (InvalidPullRequestReviewersException) e;
                Map<String, KeyedMessage> errors = ipe.getReviewerErrors();
                for (Map.Entry<String, KeyedMessage> entry : errors.entrySet()) {
                    String username = entry.getKey();
                    String message = entry.getValue().getLocalisedMessage();
                    errorMessage += "(" + username + ") " + message + "\n";
                }

            } else {
                errorMessage += e.getMessage();
            }

            sshShellHandle.writeError(errorMessage);
            logStacktrace(e);
        }
        catch (IOException ioException) {
            logStacktrace(ioException);
        }

    }


    protected void logStacktrace(Exception e) {
    	StringWriter sw = new StringWriter();
    	PrintWriter pw = new PrintWriter(sw);
    	e.printStackTrace(pw);
    	String stacktrace = sw.toString();
    
    	log.error(stacktrace);
    }

    @Override
    public void cancel() {

    }

    @Override
    public boolean isWrite() {
        return false;
    }

    @Override
    public void sendError(@Nonnull String s, @Nonnull String s2) throws IOException {

    }
}
