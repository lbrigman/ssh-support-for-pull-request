package de.aeffle.stash.plugins.ssh.json;

/*
 * Reviewer for the ArrayList in the Reviewers of PullRequest
 * 
 * Looks like this in json:
 * reviewers': [{'user': {'name': 'charlie'}}] }
 */
public class JsonReviewer {
	User user;

	public class User {
		String name;
	}

	@Override
	public String toString() {
		return user.name;
	}
}