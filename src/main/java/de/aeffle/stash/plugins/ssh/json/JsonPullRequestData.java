package de.aeffle.stash.plugins.ssh.json;

import java.util.Set;

/*
 * PullRequest
 */
public class JsonPullRequestData {
	public String title;
	public String description;
	public String state;
	
	public JsonReference fromRef;
	public JsonReference toRef;

	public Set<JsonReviewer> reviewers;

	// GSON sets the fields directly using reflection.

	public boolean isInformationComplete() {
		boolean infoComplete = false;
		if ( this.title != null 
			&& this.fromRef != null
			&& this.fromRef.infoComplete()
			&& this.toRef != null
			&& this.toRef.infoComplete()
		) {
		infoComplete = true;
		}
		return infoComplete;
	}
	
	@Override
	public String toString() {
		return title + " - " + fromRef + " - " + toRef + " - " + reviewers;
	}

}