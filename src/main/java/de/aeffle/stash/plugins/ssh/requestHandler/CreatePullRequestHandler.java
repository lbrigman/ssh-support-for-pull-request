package de.aeffle.stash.plugins.ssh.requestHandler;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import com.atlassian.bitbucket.pull.PullRequest;
import com.atlassian.bitbucket.pull.PullRequestService;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.repository.RepositoryService;
import com.google.gson.Gson;

import de.aeffle.stash.plugins.ssh.exception.IncompletePullRequestInformationException;
import de.aeffle.stash.plugins.ssh.exception.UnspecifiedPullRequestSourceException;
import de.aeffle.stash.plugins.ssh.exception.UnspecifiedPullRequestTargetException;
import de.aeffle.stash.plugins.ssh.helper.SshShellHandle;
import de.aeffle.stash.plugins.ssh.helper.BitbucketServiceHandler;
import de.aeffle.stash.plugins.ssh.json.JsonReviewer;
import de.aeffle.stash.plugins.ssh.json.JsonPullRequestData;
import de.aeffle.stash.plugins.ssh.json.JsonReference.MyRepository;

import javax.annotation.Nonnull;


public class CreatePullRequestHandler extends AbstractRequestHandler {
    private JsonPullRequestData pullRequestData;

    public CreatePullRequestHandler(String remoteCommand,
                                    SshShellHandle sshShellHandle,
                                    BitbucketServiceHandler bitbucketServiceHandler) {

        super(remoteCommand, sshShellHandle, bitbucketServiceHandler);

        try {
            String jsonData = sshShellHandle.read();
            pullRequestData = getPullRequestDataFromJson(jsonData);
        }
        catch (Exception e) {
            handleError(e);
        }
    }

    
    @Override
    public void handleRequest() throws IOException {
        try {
            PullRequest pullRequest = tryPullRequestCreation(pullRequestData);
            sshShellHandle.writeOutput("Successfully created the pull-request");

            String pullRequestUrl = getPullRequestUrl(pullRequest);
            sshShellHandle.writeOutput("URL: " + pullRequestUrl);
        } 
        catch (Exception e) {
            handleError(e);
        }
        
    }


    public JsonPullRequestData getPullRequestDataFromJson(String pullRequestJsonData)
            throws IOException {
        /*
         * Gson API Documentation: https://sites.google.com/site/gson/gson-user-guide
         * 
         * STASH REST pull-request:
         * https://developer.atlassian.com/static/rest/stash/2.7.1/stash-rest.html#idp1497024
         */
        Gson gson = new Gson();

        return gson.fromJson(pullRequestJsonData, JsonPullRequestData.class);
    }


    private PullRequest tryPullRequestCreation(JsonPullRequestData pullRequest) throws IOException {
        checkForCompletePullRequestData(pullRequest); 
        writeToSshOutput(pullRequest);

        Repository fromRepo = getSourceRepository(pullRequest);
        Repository toRepo = getDestinationRepository(pullRequest);
        Set<String> reviewers = getReviewers(pullRequest);
 
        return createPullRequestInStash(pullRequest, fromRepo, toRepo, reviewers);

    }


    private PullRequest createPullRequestInStash(JsonPullRequestData pullRequest,
        Repository fromRepo, Repository toRepo, Set<String> reviewers) {

        PullRequestService pullRequestService = bitbucketServiceHandler.getPullRequestService();
        
        return pullRequestService.create(pullRequest.title, pullRequest.description, 
                reviewers, 
                fromRepo, pullRequest.fromRef.id, 
                toRepo, pullRequest.toRef.id);
    }


    private void writeToSshOutput(JsonPullRequestData pullRequest) throws IOException {
        sshShellHandle.writeOutput("Received the following pull request:"); 
        sshShellHandle.writeOutput("Title: " + pullRequest.title);
        sshShellHandle.writeOutput("From: " + pullRequest.fromRef); 
        sshShellHandle.writeOutput("To: " + pullRequest.toRef);
    }


    private void checkForCompletePullRequestData(JsonPullRequestData pullRequest) {
        if ( !pullRequest.isInformationComplete() ) {
            throw new IncompletePullRequestInformationException();
        }
    }


    private Repository getDestinationRepository(JsonPullRequestData pullRequest) {
        MyRepository to = pullRequest.toRef.repository;
        RepositoryService repositoryService = bitbucketServiceHandler.getRepositoryService();

        Repository toRepo = repositoryService.getBySlug(to.project.key, to.slug);
        if (toRepo == null) {
            throw new UnspecifiedPullRequestTargetException();
        }
        return toRepo;
    }


    private Repository getSourceRepository(JsonPullRequestData pullRequest) {
        MyRepository from = pullRequest.fromRef.repository;
        RepositoryService repositoryService = bitbucketServiceHandler.getRepositoryService();

        Repository fromRepo = repositoryService.getBySlug(from.project.key, from.slug);
        if (fromRepo == null) {
            throw new UnspecifiedPullRequestSourceException();
        }
        return fromRepo;
    }


    private Set<String> getReviewers(JsonPullRequestData pullRequest) {
        Set<String> reviewers = new HashSet<String>();
        Set<JsonReviewer> jsonReviewers = pullRequest.reviewers;
        
        if (jsonReviewers != null) {
            for (JsonReviewer reviewer : jsonReviewers) {
                reviewers.add(reviewer.toString());
            }
        }

        return reviewers;
    }


    private String getPullRequestUrl(PullRequest pullRequest) {
        Repository repository = pullRequest.getToRef().getRepository();
        return bitbucketServiceHandler.getNavBuilder().
            project(repository.getProject().getKey()).
            repo(repository.getSlug()).
            pullRequest(pullRequest.getId()).
            buildAbsolute();
    }


    @Nonnull
    @Override
    public Repository getRepository() {
        log.warn("getRepository was called in CreatePullRequestHandler.java");
        return getSourceRepository(pullRequestData);
    }
}
