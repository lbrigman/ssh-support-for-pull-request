package de.aeffle.stash.plugins.ssh.requestHandler;

import java.io.IOException;

import com.atlassian.bitbucket.pull.PullRequestDeclineRequest;
import com.atlassian.bitbucket.pull.PullRequestService;
import com.atlassian.bitbucket.repository.Repository;
import de.aeffle.stash.plugins.ssh.helper.DeclineCommand;
import de.aeffle.stash.plugins.ssh.helper.SshShellHandle;
import de.aeffle.stash.plugins.ssh.helper.BitbucketServiceHandler;

import javax.annotation.Nonnull;


public class DeclinePullRequestHandler extends AbstractRequestHandler {
    private int repositoryId;
    private long pullRequestId;
    private int version;

    public DeclinePullRequestHandler(String remoteCommand,
            SshShellHandle sshShellHandle,
            BitbucketServiceHandler bitbucketServiceHandler) {

        super(remoteCommand, sshShellHandle, bitbucketServiceHandler);

        try {
            DeclineCommand declineCommand = new DeclineCommand(bitbucketServiceHandler, remoteCommand);

            repositoryId = declineCommand.getRepositoryId();
            pullRequestId = declineCommand.getPullRequestId();
            version = declineCommand.getPullRequestVersion();
        }
        catch (Exception e) {
            handleError(e);
        }

    }


    @Override
    public void handleRequest() throws IOException {
        try {
            declinePullRequest(repositoryId, pullRequestId, version);
            sshShellHandle.writeOutput("Successfully declined the pull-request.");
        } 
        catch (Exception e) {
            handleError(e);
        }
        
    }


    private void declinePullRequest(int repositoryId, long pullRequestId, int version) {
        PullRequestService pullRequestService = bitbucketServiceHandler.getPullRequestService();
        PullRequestDeclineRequest pullRequestDeclineRequest = new PullRequestDeclineRequest.Builder(repositoryId, pullRequestId, version).build();
        pullRequestService.decline(pullRequestDeclineRequest);
    }


    @Nonnull
    @Override
    public Repository getRepository() {
        return repositoryService.getById(repositoryId);
    }

}
