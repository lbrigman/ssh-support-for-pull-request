package de.aeffle.stash.plugins.ssh.helper;

import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.repository.RepositoryService;

import de.aeffle.stash.plugins.ssh.exception.BadNumberOfArgumentsException;
import de.aeffle.stash.plugins.ssh.exception.InvalidPullRequestIdException;

public class CommentCommand {
    private BitbucketServiceHandler bitbucketServiceHandler;
    private String projectKey;
    private String repositorySlug;
    private long pullRequestId;

    public CommentCommand(BitbucketServiceHandler bitbucketServiceHandler, String remoteCommand) {
        this.bitbucketServiceHandler = bitbucketServiceHandler;
        setCommandParameters(remoteCommand);
    }

    private void setCommandParameters(String remoteCommand) {
        String command = removePrefixSshCommand(remoteCommand);
        String[] parameter = command.trim().split("\\s+");
        
        if (parameter.length != 3) 
            throw new BadNumberOfArgumentsException();
        
        this.projectKey = parameter[0];
        this.repositorySlug = parameter[1];
        
        try {
            this.pullRequestId = Long.parseLong(parameter[2]);
        }
        catch (Exception e) {
            throw new InvalidPullRequestIdException();
        }
    }


    private String removePrefixSshCommand(String remoteCommand) {
        return remoteCommand.replaceFirst(SshRequestCommand.COMMENT_PULL_REQUEST.toString(), "");
    }


    public int getRepositoryId() {
        RepositoryService repositoryService = bitbucketServiceHandler.getRepositoryService();
        Repository repository = repositoryService.getBySlug(projectKey, repositorySlug);
        
        return repository.getId();
    }


    public long getPullRequestId() {
        return pullRequestId;
    }

}
