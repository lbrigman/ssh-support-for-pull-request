package de.aeffle.stash.plugins.ssh;

import com.atlassian.bitbucket.nav.NavBuilder;
import com.atlassian.bitbucket.pull.PullRequestService;
import com.atlassian.bitbucket.repository.RepositoryService;
import com.atlassian.bitbucket.scm.ssh.ExitCodeCallback;
import com.atlassian.bitbucket.scm.ssh.SshScmRequest;
import com.atlassian.bitbucket.scm.ssh.SshScmRequestHandler;

import de.aeffle.stash.plugins.ssh.helper.SshRequestCommand;
import de.aeffle.stash.plugins.ssh.helper.SshShellHandle;
import de.aeffle.stash.plugins.ssh.helper.BitbucketServiceHandler;
import de.aeffle.stash.plugins.ssh.requestHandler.CommentPullRequestHandler;
import de.aeffle.stash.plugins.ssh.requestHandler.CreatePullRequestHandler;
import de.aeffle.stash.plugins.ssh.requestHandler.DeclinePullRequestHandler;
import de.aeffle.stash.plugins.ssh.requestHandler.MergePullRequestHandler;

import javax.annotation.Nonnull;
import java.util.Optional;

import java.io.InputStream;
import java.io.OutputStream;


public class SshRequestHandler implements SshScmRequestHandler {
	private final PullRequestService pullRequestService;
	private final RepositoryService repositoryService;
	private final NavBuilder navBuilder;
	
	public SshRequestHandler(PullRequestService pullRequestService,
                             RepositoryService repositoryService,
                             NavBuilder navBuilder) {

		this.pullRequestService = pullRequestService;
		this.repositoryService = repositoryService;
		this.navBuilder = navBuilder;
	}


    @Nonnull
    @Override
    public Optional<SshScmRequest> create(@Nonnull String remoteCommand,
                                          @Nonnull InputStream inputStream,
                                          @Nonnull OutputStream outputStream,
                                          @Nonnull OutputStream errorStream,
                                          @Nonnull ExitCodeCallback exitCodeCallback) {


        SshShellHandle sshShellHandle = new SshShellHandle(inputStream, outputStream, errorStream, exitCodeCallback);
        BitbucketServiceHandler bitbucketServiceHandler = new BitbucketServiceHandler();
        bitbucketServiceHandler.add(pullRequestService);
        bitbucketServiceHandler.add(repositoryService);
        bitbucketServiceHandler.add(navBuilder);

        SshScmRequest result = getSshScmRequestByCommand(remoteCommand, sshShellHandle, bitbucketServiceHandler);
        return Optional.ofNullable(result);
    }

    @Override
    public boolean supports(@Nonnull String remoteCommand) {
        if (SshRequestCommand.PULL_REQUEST.isMatchFor(remoteCommand))
            return true;

        if (SshRequestCommand.DECLINE_PULL_REQUEST.isMatchFor(remoteCommand))
            return true;

        if (SshRequestCommand.COMMENT_PULL_REQUEST.isMatchFor(remoteCommand))
            return true;

        if (SshRequestCommand.MERGE_PULL_REQUEST.isMatchFor(remoteCommand))
            return true;

        return false;
    }


    private SshScmRequest getSshScmRequestByCommand(String remoteCommand,
            SshShellHandle sshShellHandle,
            BitbucketServiceHandler bitbucketServiceHandler) {
        if (SshRequestCommand.PULL_REQUEST.isMatchFor(remoteCommand))
	        return new CreatePullRequestHandler(remoteCommand, sshShellHandle, bitbucketServiceHandler);
	        
		if (SshRequestCommand.DECLINE_PULL_REQUEST.isMatchFor(remoteCommand)) 
            return new DeclinePullRequestHandler(remoteCommand, sshShellHandle, bitbucketServiceHandler);

		if (SshRequestCommand.COMMENT_PULL_REQUEST.isMatchFor(remoteCommand))
		    return new CommentPullRequestHandler(remoteCommand, sshShellHandle, bitbucketServiceHandler);

		if (SshRequestCommand.MERGE_PULL_REQUEST.isMatchFor(remoteCommand))
		    return new MergePullRequestHandler(remoteCommand, sshShellHandle, bitbucketServiceHandler);
		            
		return commandNotSupported();
    }
	
	private SshScmRequest commandNotSupported() {
	    return null;
	}
}
