package de.aeffle.stash.plugins.ssh.exception;

public class UnspecifiedPullRequestSourceException extends RuntimeException {
    private static final long serialVersionUID = 1L;
    private static String errorMessage = "The source repository ('from') could not be found.";
    
    public UnspecifiedPullRequestSourceException() {
        super(errorMessage );
    }
    
}
