package de.aeffle.stash.plugins.ssh.requestHandler;

import java.io.IOException;
import com.atlassian.bitbucket.pull.PullRequestService;
import com.atlassian.bitbucket.repository.Repository;
import com.google.gson.Gson;
import de.aeffle.stash.plugins.ssh.helper.CommentCommand;
import de.aeffle.stash.plugins.ssh.helper.SshShellHandle;
import de.aeffle.stash.plugins.ssh.helper.BitbucketServiceHandler;
import de.aeffle.stash.plugins.ssh.json.JsonComment;

import javax.annotation.Nonnull;


public class CommentPullRequestHandler extends AbstractRequestHandler {
    private int repositoryId;
    private long pullRequestId;

    public CommentPullRequestHandler(String remoteCommand,
                                     SshShellHandle sshShellHandle,
                                     BitbucketServiceHandler bitbucketServiceHandler) {

        super(remoteCommand, sshShellHandle, bitbucketServiceHandler);

        try {
            CommentCommand commentCommand = new CommentCommand(bitbucketServiceHandler, remoteCommand);

            repositoryId = commentCommand.getRepositoryId();
            pullRequestId = commentCommand.getPullRequestId();
        }
        catch (Exception e) {
            handleError(e);
        }
    }


    @Override
    public void handleRequest() throws IOException {
        try {
            String jsonData = sshShellHandle.read();
            JsonComment comment = getCommentFromJson(jsonData);
            commentPullRequest(repositoryId, pullRequestId, comment);
            sshShellHandle.writeOutput("Successfully commented on the pull-request.");
        } 
        catch (Exception e) {
            handleError(e);
        }
    }


    public JsonComment getCommentFromJson(String jsonData) throws IOException {
        // Gson API Documentation: https://sites.google.com/site/gson/gson-user-guide
        Gson gson = new Gson();
        return gson.fromJson(jsonData, JsonComment.class);
    }


    private void commentPullRequest(int repositoryId, long pullRequestId, JsonComment comment) {
        PullRequestService pullRequestService = bitbucketServiceHandler.getPullRequestService();
        pullRequestService.addComment(repositoryId, pullRequestId, comment.toString());
    }


    @Nonnull
    @Override
    public Repository getRepository() {
        return repositoryService.getById(repositoryId);
    }


}
