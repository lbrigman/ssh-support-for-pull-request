package de.aeffle.stash.plugins.ssh.helper;

public enum SshRequestCommand {
    PULL_REQUEST("pull-request"),
    DECLINE_PULL_REQUEST("decline pull-request"),
    COMMENT_PULL_REQUEST("comment pull-request"),
    MERGE_PULL_REQUEST("merge pull-request");

    private String command;
    
    SshRequestCommand(String command) {
        this.command = command.toLowerCase();
    }

    public boolean isMatchFor(String command) {
        return command.toLowerCase().startsWith(this.command);
    }
    
    public String toString() {
        return command;
    }
}
