package de.aeffle.stash.plugins.ssh.helper;

import com.atlassian.bitbucket.pull.PullRequest;
import com.atlassian.bitbucket.pull.PullRequestService;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.repository.RepositoryService;

import de.aeffle.stash.plugins.ssh.exception.BadNumberOfArgumentsException;
import de.aeffle.stash.plugins.ssh.exception.InvalidPullRequestIdException;

public class MergeCommand {
    private BitbucketServiceHandler bitbucketServiceHandler;
    private String projectKey;
    private String repositorySlug;
    private long pullRequestId;
    private int pullRequestVersion;

    public MergeCommand(BitbucketServiceHandler bitbucketServiceHandler, String remoteCommand) {
        this.bitbucketServiceHandler = bitbucketServiceHandler;
        setCommandParameters(remoteCommand);
    }

    private void setCommandParameters(String remoteCommand) {
        String command = removePrefixSshCommand(remoteCommand);
        String[] parameter = command.trim().split("\\s+");
        
        if (parameter.length < 3 || parameter.length > 4) 
            throw new BadNumberOfArgumentsException();
        
        this.projectKey = parameter[0];
        this.repositorySlug = parameter[1];
        this.pullRequestId = getPullRequestId(parameter);
        
        if (parameter.length == 3) {
            this.pullRequestVersion = getCurrentPullRequestVersion();
        }
        
        if (parameter.length == 4) {
            this.pullRequestVersion = getPullRequestVersion(parameter);
        }
    }

    private int getCurrentPullRequestVersion() {
        PullRequestService pullRequestService = bitbucketServiceHandler.getPullRequestService();
        int repositoryId = getRepositoryId();
        PullRequest pullRequest = pullRequestService.getById(repositoryId, pullRequestId);
        
        return pullRequest.getVersion();
    }

    private long getPullRequestId(String[] parameter) {
        try {
            return Long.parseLong(parameter[2]);
        }
        catch (Exception e) {
            throw new InvalidPullRequestIdException();
        }
    }

    private int getPullRequestVersion(String[] parameter) {
        try {
            return Integer.parseInt(parameter[3]);
        }
        catch (Exception e) {
            throw new InvalidPullRequestIdException();
        }
    }

    private String removePrefixSshCommand(String remoteCommand) {
        return remoteCommand.replaceFirst(SshRequestCommand.MERGE_PULL_REQUEST.toString(), "");
    }


    public int getRepositoryId() {
        RepositoryService repositoryService = bitbucketServiceHandler.getRepositoryService();
        Repository repository = repositoryService.getBySlug(projectKey, repositorySlug);
        
        return repository.getId();
    }


    public long getPullRequestId() {
        return pullRequestId;
    }
    
    public int getPullRequestVersion() {
        return pullRequestVersion;
    }

}
